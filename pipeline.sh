#!/bin/sh

set -e -x

mkdir -p /tmp/pipeline
cd /tmp/pipeline
rm -rf $version
git clone $source $version

cd $version/code
docker-compose -p $version build
docker-compose -p $version run unit-test
docker-compose -p $version run functional-test
docker-compose -p $version up -d web
docker port ${version}_web_1

docker-compose -p ${version}preprod -f docker/preprod.yml build
docker-compose -p ${version}preprod -f docker-compose.yml \
    -f docker/preprod.yml run functional-test
docker-compose -p ${version}preprod -f docker-compose.yml \
    -f docker/preprod.yml up -d web
docker port ${version}preprod_web_1
