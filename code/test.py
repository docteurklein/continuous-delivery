
from app import hello, redis

redis.incr = lambda x: x
redis.get = lambda x: 'mocked'

if hello() != 'Hello World! I have been seen mocked times.':
    exit(1)

print('ok')
exit(0)
