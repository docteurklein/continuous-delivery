# continuous delivery workshop

## install

    # docker toolbox in setup/*
    docker load -i setup/redis.tar
    docker load -i setup/python.tar

## run

    source=$(pwd) version=$(git rev-parse HEAD) ./pipeline.sh
